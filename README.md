# Ejemplo de uso de MPU para STM32F401

Tomo el ejemplo escrito por @pridolfi en [https://github.com/pridolfi/workspace/tree/master/examples/mpu](https://github.com/pridolfi/workspace/tree/master/examples/mpu) y lo paso al [STM32F401RE](https://www.st.com/en/microcontrollers-microprocessors/stm32f401re.html) más específicamente a la placa [Nucleo-F401RE](https://www.st.com/en/evaluation-tools/nucleo-f401re.html) porque el [STM32F103C8](https://www.st.com/en/microcontrollers-microprocessors/stm32f103c8.html) no tiene MPU.

El ejemplo define dos zonas de memoria. 
* Una zona de memoria de código de 512KB que se solapa completamente con la memoria Flash del microcontrolador. 
* Una zona de memoria RAM interna de 64KB que se solapa con los primeros 64KB de de la RAM del microcontrolador (tiene 96KB). También modifico https://gitlab.com/asm_cortex_m3/ejemplo_mpu_401/-/blob/master/hola_mpu_401/STM32F401RETX_FLASH.ld y https://gitlab.com/asm_cortex_m3/ejemplo_mpu_401/-/blob/master/hola_mpu_401/STM32F401RETX_RAM.ld para que todo el programa sólo use los primeros 64KB de RAM.

Luego, se inicia el programa, se pasa a modo usuario y trata de acceder a la memoria RAM que está por encima de los 64KB generando una excepción por MemFault debido a la configuración de la MPU. En modo thread se accede a esa zona de memoria sin problemas para terminar la prueba.
